import 'package:control_game/ui/treinos/treinos_new.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TreinoHomePage extends StatefulWidget {
  @override
  _TreinoHomePageState createState() => _TreinoHomePageState();
}

class _TreinoHomePageState extends State<TreinoHomePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Text(
                  "Treinos",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                ),
              ),
              Divider()
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              card(Icon(Icons.star_border), "08/09/2019"),
              card(Icon(Icons.star_border), "05/09/2019"),
              card(Icon(Icons.star), "01/09/2019"),
              cardMore(),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Adicionar Novo",
                      style: TextStyle(fontSize: 16.0),
                    ),
                    IconButton(
                      icon: Icon(Icons.add_circle, size: 32.0,),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TreinoNew()),
                        );
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    "Opções",
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Divider()
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              card(Icon(FontAwesomeIcons.trophy), "Melhores Marcas"),
              card(Icon(FontAwesomeIcons.chartLine), "Ver Progresso"),
              cardMore(),
            ],
          ),
          Container(height: 20.0,)
        ],
      ),
    ));
  }

  Widget card(Icon fav, String data) {
    return Container(
      height: 60.0,
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              IconButton(
                icon: fav,
                iconSize: 30.0,
                onPressed: () {},
              ),
              Padding(
                  padding: EdgeInsets.only(left: 5.0),
                  child: Container(
                    width: 160.0,
                    child: Text(
                      data,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  )),
              //              child: Image.network(img),
              Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.remove_red_eye), onPressed: () {}),
                      Padding(
                        padding: EdgeInsets.only(left: 1.0),
                        child: IconButton(
                            icon: Icon(Icons.edit), onPressed: () {}),
                      )
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget cardMore() {
    return Container(
      height: 60.0,
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.expand_more),
                iconSize: 30.0,
                onPressed: () {},
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Text(
                  "Ver Todas",
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
              //              child: Image.network(img),
            ],
          ),
        ),
      ),
    );
  }
}
