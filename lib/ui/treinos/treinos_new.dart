import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TreinoNew extends StatefulWidget {
  @override
  _TreinoNewState createState() => _TreinoNewState();
}

class _TreinoNewState extends State<TreinoNew> {
  var localController = TextEditingController();
  var descricaoController = TextEditingController();
  var dataController = TextEditingController();
  var categoriaController = TextEditingController();

  List<bool> list = [
    false, //0 - erros
    false, //1 - rebotes
    false, //2 - assist
    false, //3 - blocks
    false, //4 - turnovers
    false, //5 - faltas
  ];

  bool showMore = false;
  bool checkAll = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dataController.text = "";
    localController.text = "Clube Brilhante";
    descricaoController.text = "Treino realizado no ...";
    categoriaController.text = "Sub20";
  }

  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        dataController.text = picked.day.toString() +
            "-" +
            picked.month.toString() +
            "-" +
            picked.year.toString();
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.account_circle),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
              ),
              Text("Willian")
            ],
          ),
          centerTitle: true,
        ),
//        drawer: MenuLateral(),
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text(
                  "Novo Treino",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                ),
              ),
              Divider(),
              Container(
                  height: 60.0,
                  child: Card(
                      elevation: 2.0,
                      child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: TextField(
                            textAlign: TextAlign.end,
                            controller: dataController,
                            onTap: () {
                              _selectDate(context);
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.calendar_today),
                              labelText: "Data do Treino",
                            ),
                          )))),
              card("Local", localController, Icon(Icons.location_on)),
              card("Descrição", descricaoController,
                  Icon(Icons.import_contacts)),
              card("Categoria", categoriaController, Icon(Icons.group)),
              Divider(),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 260.0,
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Opções",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10.0),
                            child: Icon(Icons.playlist_add_check),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Checkbox(
                          value: checkAll,
                          onChanged: (bool val) {
                            setState(() {
                              checkAll = !checkAll;
                              for(int i = 0;i < list.length;i++){
                                list[i] = !list[i];
                              }
                            });
                          },
                        ))
                  ],
                ),
              ),
              Divider(),
              cardCheckBox(
                  "Acertos e Erros",
                  0,
                  Icon(
                    Icons.check_circle_outline,
                  )),
              cardCheckBox("Rebotes", 1, Icon(Icons.check_circle_outline)),
              cardCheckBox("Assistências", 2, Icon(Icons.check_circle_outline)),
              showMore == true
                  ? Column(
                      children: <Widget>[
                        cardCheckBox(
                            "Bloqueios/Tocos",
                            3,
                            Icon(
                              Icons.check_circle_outline,
                            )),
                        cardCheckBox(
                            "Turnovers", 4, Icon(Icons.check_circle_outline)),
                        cardCheckBox(
                            "Faltas", 5, Icon(Icons.check_circle_outline))
                      ],
                    )
                  : Container(),
              cardMore(),
              Divider(),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          minWidth: 120.0,
                          highlightElevation: 2,
                          splashColor: Colors.black,
                          child: Text('Criar',
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 20.0)),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.blue)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: MaterialButton(
                          onPressed: () {},
                          minWidth: 120.0,
                          highlightElevation: 2,
                          splashColor: Colors.black,
                          child: Text('Cancelar',
                              style: TextStyle(
                                  color: Colors.redAccent, fontSize: 20.0)),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.red)),
                        ),
                      ),
                    ],
                  )),
              Container(
                height: 20.0,
              )
            ],
          ),
        )));
  }

  Widget card(String text, TextEditingController controller, Icon ic) {
    return Container(
        height: 60.0,
        child: Card(
            elevation: 1.0,
            child: Padding(
                padding: EdgeInsets.all(5.0),
                child: TextField(
                  textAlign: TextAlign.end,
                  controller: controller,
                  onTap: () {},
                  decoration: InputDecoration(
                    prefixIcon: ic,
                    labelText: text,
                  ),
                ))));
  }



  Widget cardCheckBox(String text, int id, Icon ic) {
    return Container(
        height: 60.0,
        child: Card(
            elevation: 1.0,
            child: Padding(
                padding: EdgeInsets.all(5.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 250.0,
                      child: Row(
                        children: <Widget>[
                          ic,
                          Padding(
                            padding: EdgeInsets.only(left: 10.0),
                            child: Text(text),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Checkbox(
                          value: list[id],
                          onChanged: (bool val) {
                            setState(() {
                              list[id] = !list[id];
                            });
                          },
                        ))
                  ],
                ))));
  }

  Widget cardMore() {
    return GestureDetector(
      onTap: (){
        setState(() {
          showMore = !showMore;
        });
      },
        child: Container(
      height: 60.0,
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: showMore == true
                    ? Icon(Icons.expand_less)
                    : Icon(Icons.expand_more),
                iconSize: 30.0,
                onPressed: () {
                  setState(() {
                    showMore = !showMore;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Text(
                  showMore == true
                      ? "Ver Menos"
                      : "Ver Mais",
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
              //              child: Image.network(img),
            ],
          ),
        ),
      ),
    ));
  }
}
