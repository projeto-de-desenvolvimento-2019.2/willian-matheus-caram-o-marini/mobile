import 'package:control_game/ui/menu/menu_lateral.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditPerfil extends StatefulWidget {
  @override
  _EditPerfilState createState() => _EditPerfilState();
}

class _EditPerfilState extends State<EditPerfil> {

  var nomeController = TextEditingController();
  var apelidoController = TextEditingController();
  var dataController = TextEditingController();
  var emailController = TextEditingController();
  var senhaController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nomeController.text = "Willian Marini";
    apelidoController.text = "Cubo";
    dataController.text = "13/fev/2019";
    emailController.text = "willianmatheuscm@gmail.com";
    senhaController.text = "123@123";
  }

  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        dataController.text = picked.day.toString() + "-" + picked.month.toString() + "-" + picked.year.toString();
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.account_circle),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
              ),
              Text("Willian")
            ],
          ),
          centerTitle: true,
        ),
//        drawer: MenuLateral(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text(
                  "Editar Perfil",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                ),
              ),
              Icon(
                Icons.account_circle,
                size: 180.0,
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    controller: nomeController,
                    decoration: InputDecoration(
                      suffixIcon: Icon(FontAwesomeIcons.solidUserCircle),
                      hintText: "Nome",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    controller: apelidoController,
                    decoration: InputDecoration(
                      suffixIcon: Icon(FontAwesomeIcons.solidUserCircle),
                      hintText: "Apelido",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    onTap: (){
                      _selectDate(context);
                    },
                    controller: dataController,
                    decoration: InputDecoration(
                      suffixIcon: Icon(FontAwesomeIcons.calendarWeek),
                      hintText: "Nascimento",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.mail_outline),
                      hintText: "E-mail",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    controller: senhaController,
                    obscureText: true,
                    decoration: InputDecoration(
                      suffixIcon: Icon(FontAwesomeIcons.key),
                      hintText: "Senha",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.expand_more),
                      hintText: "Altura",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.expand_more),
                      hintText: "Peso",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: TextField(
                    decoration: InputDecoration(
                      suffixIcon: Icon(FontAwesomeIcons.list),
                      hintText: "Posição",
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          minWidth: 120.0,
                          highlightElevation: 2,
                          splashColor: Colors.black,
                          child: Text('Salvar',
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 20.0)),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.blue)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: MaterialButton(
                          onPressed: () {},
                          minWidth: 120.0,
                          highlightElevation: 2,
                          splashColor: Colors.black,
                          child: Text('Cancelar',
                              style: TextStyle(
                                  color: Colors.redAccent, fontSize: 20.0)),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.red)),
                        ),
                      ),
                    ],
                  )),
              Container(
                height: 20.0,
              )

            ],
          ),
        ));
  }
}
