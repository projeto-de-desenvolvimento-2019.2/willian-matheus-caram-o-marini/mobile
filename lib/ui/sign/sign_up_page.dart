import 'package:control_game/ui/menu/menu_inferior.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var dataController = TextEditingController();
  bool value = false;

  bool senha = true;
  bool ConfirmaSenha = true;

  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        dataController.text = picked.day.toString() + "-" + picked.month.toString() + "-" + picked.year.toString();
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(FontAwesomeIcons.solidUserCircle),
                    hintText: "Nome(*)",
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Sobrenome(*)",
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.person_outline),
                    hintText: "Apelido",
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    suffixIcon: Icon(FontAwesomeIcons.envelope),
                    hintText: "E-mail(*)",
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  obscureText: senha,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    hintText: "Senha(*)",
                    suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            senha = !senha;
                          });
                        },
                        icon: senha == true
                            ? Icon(Icons.visibility_off)
                            : Icon(Icons.visibility)),
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  obscureText: ConfirmaSenha,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    hintText: "Confirme a Senha(*)",
                    suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            ConfirmaSenha = !ConfirmaSenha;
                          });
                        },
                        icon: ConfirmaSenha == true
                            ? Icon(Icons.visibility_off)
                            : Icon(Icons.visibility)),
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: TextField(
                  onTap: ()=> _selectDate(context),
                  controller: dataController,
                  keyboardType: TextInputType.datetime,
                  decoration: InputDecoration(
                    hintText: "Data de Nascimento",
                    suffixIcon: Icon(FontAwesomeIcons.calendarCheck),
                  ),
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: CheckboxListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text("Li e aceito os termos de uso"),
                  value: value,
                  onChanged: (bool val) {
                    setState(() {
                      value = val;
                    });
                  },
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MenusPage()),
                          );
                        },
                        minWidth: 120.0,
                        highlightElevation: 2,
                        splashColor: Colors.black,
                        child: Text('Cadastrar',
                            style: TextStyle(
                                color: Colors.blueAccent, fontSize: 20.0)),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.blue)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: MaterialButton(
                        onPressed: () {},
                        minWidth: 120.0,
                        highlightElevation: 2,
                        splashColor: Colors.black,
                        child: Text('Limpar',
                            style: TextStyle(
                                color: Colors.redAccent, fontSize: 20.0)),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.red)),
                      ),
                    ),
                  ],
                )),
            Container(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }
}
