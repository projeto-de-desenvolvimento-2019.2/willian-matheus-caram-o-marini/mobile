import 'package:control_game/ui/menu_pages/edit_perfil.dart';
import 'package:control_game/ui/sign/sign_in_page.dart';
import 'package:flutter/material.dart';

class MenuLateral extends StatefulWidget {
  @override
  _MenuLateralState createState() => _MenuLateralState();
}

class _MenuLateralState extends State<MenuLateral> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: SingleChildScrollView(
      child: Column(
//        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('Willian'),
            accountEmail: Text('willianmatheuscm@gmail.com'),
            currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("images/logotipo.png")),
          ),
          SingleChildScrollView(
            child: Wrap(
              runSpacing: -10.0,
              children: <Widget>[
                tileObject("Editar Perfil", Icon(Icons.settings), EditPerfil()),
                ListTile(
                  title: Text("Grupos"),
                ),
                tileObject("New Gera", Icon(Icons.star), EditPerfil()),
                tileObject("Brilhante", Icon(Icons.star_border), EditPerfil()),
                tileObject(
                    "Novo", Icon(Icons.add_circle_outline), EditPerfil()),
                ListTile(
                  title: Text("Treinos"),
                ),
                tileObject(
                    "Ver Todos", Icon(Icons.import_contacts), EditPerfil()),
                tileObject(
                    "Novo", Icon(Icons.add_circle_outline), EditPerfil()),
                ListTile(
                  title: Text("Sobre o Aplicativo"),
                ),
                tileObject("Dicas", Icon(Icons.notifications), EditPerfil()),
                tileObject(
                    "Termos de Uso", Icon(Icons.assistant_photo), EditPerfil()),
                tileObject("Fale Conosco", Icon(Icons.textsms), EditPerfil()),
                tileObject("Sair", Icon(Icons.cancel), SignInPage()),
              ],
            ),
          )
        ],
      ),
    ));
  }

  Widget tileObject(String text, Icon icon, Widget rota) {
    return ListTile(
      title: Row(
        children: <Widget>[
          IconButton(
            onPressed: () {},
            icon: icon,
          ),
          Text(text)
        ],
      ),
      onTap: () {
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (BuildContext context) => rota));
      },
    );
  }
}
